package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_registro.*

class registro : AppCompatActivity() {
    var n = 1
    var i = 0
    var datos1 = arrayOfNulls<String>(n)
    var datos2 = arrayOfNulls<String>(n)
    var datos3 = arrayOfNulls<String>(n)
    var datos4 = arrayOfNulls<String>(n)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        btn_guardar.setOnClickListener {
            val nombres = nom.text.toString()
            val apellidos = apellidos.text.toString()
            val us = us.text.toString()
            val contra = con.text.toString()

            if (nombres.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (apellidos.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (us.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (contra.isEmpty()) {
                Toast.makeText(this, "Debes ingresar todos los datos", Toast.LENGTH_SHORT).show()
            }
            if (!chkTerminos.isChecked) {
                Toast.makeText(this, "Debe aceptar los terminos", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            datos1[i] = nombres
            datos2[i] = apellidos
            datos3[i] = us
            datos4[i] = contra
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres", nombres)
                putString("key_apellido", apellidos)
                putString("key_us", us)
                putString("key_contra1", contra)
            }
            Toast.makeText(this, "Registro completado", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, login::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }

    }
}